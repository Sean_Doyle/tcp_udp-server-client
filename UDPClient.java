
import java.io.*;
import java.net.*;
import java.util.*;

public class UDPClient {
    public static void main(String[] args) throws IOException {

        if (args.length != 1) {
             System.out.println("Usage: java UDPClient <hostname>");
             return;
        }
		
			// get a datagram socket
		DatagramSocket socket = new DatagramSocket();
		
		System.out.println("Would You Like To Recieve a Famous Quote?(Y/N)");
		while(true){
			

			
			BufferedReader stdIn = new BufferedReader(
							new InputStreamReader(System.in));
			String userInput;
			userInput = stdIn.readLine();
			userInput = userInput.toLowerCase();
			if(userInput.equals("y")){
					// send request
				byte[] buf = new byte[256];
				InetAddress address = InetAddress.getByName(args[0]);
				DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 4445);
				socket.send(packet);
			
					// get response
				packet = new DatagramPacket(buf, buf.length);
				socket.receive(packet);

					// display response
				String received = new String(packet.getData(), 0, packet.getLength());
				System.out.println("Quote of the Moment: " + received + "\n");
				System.out.println("Would You Like To Recieve Another?(Y/N)");
			} else {
				break;
			}
		}
		socket.close();
		
    }
}